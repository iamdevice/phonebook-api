#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
        set -- php-fpm "$@"
fi

composer install -d /var/www/html -o --no-dev
wait-for-it.sh --timeout=60 --host=mysql --port=3306
/var/www/html/vendor/bin/phalcon-migrations migration --action=run --config=config/config.php

exec "$@"
