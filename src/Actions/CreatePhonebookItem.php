<?php

declare(strict_types=1);

namespace HostawayAPI\Actions;

use HostawayAPI\Models\PhonebookItem;
use HostawayAPI\Validators\PhonebookItemValidator;
use InvalidArgumentException;

final class CreatePhonebookItem
{
    private PhonebookItemValidator $validator;

    public function __construct(PhonebookItemValidator $validator)
    {
        $this->validator = $validator;
    }

    public function handle(array $payload): array
    {
        if (!$this->validator->isValid($payload)) {
            $errorMessage = json_encode($this->validator->getErrors(), JSON_THROW_ON_ERROR);

            throw new InvalidArgumentException($errorMessage);
        }

        $phonebookItem = new PhonebookItem();
        $phonebookItem->assign([
            'first_name' => $payload['firstName'],
            'last_name' => $payload['lastName'] ?? null,
            'phone_number' => $payload['phoneNumber'],
            'country_code' => $payload['countryCode'],
            'tz_name' => $payload['timezone'],
        ]);
        $phonebookItem->save();

        return $phonebookItem->jsonSerialize();
    }
}
