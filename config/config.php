<?php

declare(strict_types=1);

use Phalcon\Config;
use Symfony\Component\Dotenv\Dotenv;

$env = new Dotenv();
$env->overload(__DIR__ . '/../.env');

return new Config([
    'database' => [
        'adapter' => 'mysql',
        'host' => $_ENV['DB_HOST'] ?? 'localhost',
        'username' => $_ENV['DB_USER'] ?? null,
        'password' => $_ENV['DB_PASSWORD'] ?? null,
        'dbname' => $_ENV['DB_NAME'] ?? null,
        'port' => 3306,
        'charset' => 'utf8',
    ],
    'cache' => [
        'adapter' => 'redis',
        'options' => [
            'host' => $_ENV['REDIS_HOST'] ?? 'localhost',
            'port' => 6379,
            'index' => 0,
            'persistent' => false,
            'defaultSerializer' => 'Php',
            'prefix' => 'ha-',
        ],
    ],
    'logger' => [
        'name' => 'hostaway-api',
        'adapters' => [
            'main' => [
                'adapter' => 'stream',
                'name' => __DIR__ . '/../var/logs/app.log',
                'options' => [],
            ],
        ],
    ],
    'application' => [
        'logInDb' => true,
        'migrationsDir' => 'migrations',
        'migrationsTsBased' => false,
        'baseUri' => "/",
    ],
]);
