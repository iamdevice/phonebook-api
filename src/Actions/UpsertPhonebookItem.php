<?php

declare(strict_types=1);

namespace HostawayAPI\Actions;

use HostawayAPI\Exceptions\EntityNotFound;
use HostawayAPI\Models\PhonebookItem;
use HostawayAPI\Validators\PhonebookItemValidator;
use InvalidArgumentException;

final class UpsertPhonebookItem
{
    private PhonebookItemValidator $validator;

    public function __construct(PhonebookItemValidator $validator)
    {
        $this->validator = $validator;
    }

    public function handle(int $id, array $payload): array
    {
        if (!$this->validator->isValid($payload)) {
            $errorMessage = json_encode($this->validator->getErrors(), JSON_THROW_ON_ERROR);

            throw new InvalidArgumentException($errorMessage);
        }

        $phonebookItem = PhonebookItem::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                'id' => $id,
            ],
        ]);
        if (!$phonebookItem instanceof PhonebookItem) {
            $phonebookItem = new PhonebookItem();
        }

        $phonebookItem->assign([
            'id' => $id,
            'first_name' => $payload['firstName'],
            'last_name' => $payload['lastName'] ?? null,
            'phone_number' => $payload['phoneNumber'],
            'country_code' => $payload['countryCode'],
            'tz_name' => $payload['timezone'],
        ]);
        $phonebookItem->save();

        return $phonebookItem->jsonSerialize();
    }
}
