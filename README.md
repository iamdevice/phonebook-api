# PhoneBook API

## First run the environment

```bash
cp .env.example .env
docker-compose up --build -d
```

After that you could send requests to `http://localhost`, for example

```http request
POST /phonebook/items HTTP/1.1
Host: localhost
Content-Type: application-json

{
    "firstName": "Alexander",
    "phoneNumber": "+7 123 123 1234",
    "countryCode": "RU",
    "timezone": "Europe/Samara"
}
```

All possible HTTP requests you could see in `http-request.http`
