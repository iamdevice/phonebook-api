<?php

declare(strict_types=1);

namespace HostawayAPI\Exceptions;

use DomainException;

final class EntityNotFound extends DomainException
{

}
