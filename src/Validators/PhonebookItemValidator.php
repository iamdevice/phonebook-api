<?php

declare(strict_types=1);

namespace HostawayAPI\Validators;

use HostawayAPI\DataProviders\HostawayCountryProvider;
use HostawayAPI\DataProviders\HostawayTimezoneProvider;
use Phalcon\Validation;

final class PhonebookItemValidator
{
    private const REQUIRED_FIELDS = [
        'firstName',
        'phoneNumber',
        'countryCode',
        'timezone',
    ];

    private $countryDataProvider;

    private $timezoneDataProvider;

    private $errorMessages = [];

    public function __construct(
        HostawayCountryProvider $countryDataProvider,
        HostawayTimezoneProvider $timezoneDataProvider
    ) {
        $this->countryDataProvider = $countryDataProvider;
        $this->timezoneDataProvider = $timezoneDataProvider;
    }

    public function isValid(
        array $data,
        array $fields = ['firstName', 'lastName', 'phoneNumber', 'countryCode', 'timezone']
    ): bool {
        $this->errorMessages = [];

        $validator = new Validation();

        $validator->add(
            array_intersect(self::REQUIRED_FIELDS, $fields),
            new Validation\Validator\PresenceOf()
        );

        if (in_array('firstName', $fields, true)) {
            $validator->add(
                'firstName',
                new Validation\Validator\StringLength([
                    'min' => 4,
                    'max' => 150,
                ])
            );
        }

        if (in_array('lastName', $fields, true)) {
            $validator->add(
                'lastName',
                new Validation\Validator\StringLength([
                    'min' => 0,
                    'max' => 150,
                ])
            );
        }

        if (in_array('phoneNumber', $fields, true)) {
            $validator->add(
                'phoneNumber',
                new Validation\Validator\Regex([
                    'message' => 'phoneNumber should be valid',
                    'pattern' => '/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/' // International phone number format
                ])
            );
        }

        if (in_array('countryCode', $fields, true)) {
            $validator->add(
                'countryCode',
                new Validation\Validator\InclusionIn([
                    'domain' => $this->countryDataProvider->getCountryCodeList(),
                ])
            );
        }

        if (in_array('timezone', $fields, true)) {
            $validator->add(
                'timezone',
                new Validation\Validator\InclusionIn([
                    'domain' => $this->timezoneDataProvider->getTimezoneList(),
                ])
            );
        }

        $messages = $validator->validate($data);
        if ($messages->count()) {
            foreach ($messages as $message) {
                $this->errorMessages[$message->getField()] = $message->getMessage();
            }

            return false;
        }

        return true;
    }

    public function getErrors(): array
    {
        return $this->errorMessages;
    }
}
