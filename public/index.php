<?php

declare(strict_types=1);

use HostawayAPI\Exceptions\EntityNotFound;
use Phalcon\Mvc\Micro;

error_reporting(E_STRICT);

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config/loader.php';

$config = require(__DIR__ . '/../config/config.php');
$container = require(__DIR__ . '/../config/container.php');
$routes = require(__DIR__ . '/../config/routes.php');

$app = new Micro($container);
$app->mount($routes);

$app->notFound(static function () use ($app) {
    $app->response->setContent(json_encode(['status' => 'error', 'message' => 'Not Found'], JSON_THROW_ON_ERROR));
    $app->response->send();
});

$app->after(static function () use ($app) {
    $result = $app->getReturnedValue();

    if (is_array($result) && !empty($result)) {
        $app->response->setContent(json_encode($result, JSON_THROW_ON_ERROR));
    } elseif (empty($result)) {
        $app->response->setStatusCode(204, 'No Content');
    } else {
        throw new Exception('Something went wrong');
    }

    $app->response->send();
});

try {
    $app->handle($_SERVER['REQUEST_URI']);
} catch (EntityNotFound $e) {
    $app->response->setStatusCode(404, 'Not Found');
    $app->response->setContent(json_encode(['status' => 'success', 'message' => $e->getMessage()], JSON_THROW_ON_ERROR));
} catch (InvalidArgumentException $e) {
    $app->response->setContent(json_encode([
        'status' => 'error',
        'message' => json_decode($e->getMessage(), true, JSON_THROW_ON_ERROR),
    ], JSON_THROW_ON_ERROR));
    $app->response->setStatusCode(422, 'Unprocessable Entity');
} catch (Throwable $e) {
    $app->logger->error($e->getMessage());
    $app->logger->debug($e->getTraceAsString());
    $app->response->setStatusCode(500, 'Server Internal Error');
    $app->response->setContent(json_encode(['status' => 'error', 'message' => 'Something went wrong'], JSON_THROW_ON_ERROR));
}

if (!$app->response->isSent()) {
    $app->response->send();
}
