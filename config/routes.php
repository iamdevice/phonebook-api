<?php

declare(strict_types=1);

use HostawayAPI\Controllers\PhonebookItemController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

$phonebook = new MicroCollection();
$phonebook->setHandler(new PhonebookItemController());
$phonebook->setPrefix('/phonebook/items');

$phonebook->get('/', 'list');
$phonebook->get('/{id}', 'show');
$phonebook->post('/', 'create');
$phonebook->put('/{id}', 'upsert');
$phonebook->patch('/{id}', 'modify');
$phonebook->delete('/{id}', 'delete');

return $phonebook;
