<?php

declare(strict_types=1);

use GuzzleHttp\Client as HttpClient;
use HostawayAPI\Actions\CreatePhonebookItem;
use HostawayAPI\Actions\DeletePhonebookItem;
use HostawayAPI\Actions\ListPhonebookItem;
use HostawayAPI\Actions\ModifyPhonebookItem;
use HostawayAPI\Actions\ShowPhonebookItem;
use HostawayAPI\Actions\UpsertPhonebookItem;
use HostawayAPI\DataProviders\HostawayCountryProvider;
use HostawayAPI\DataProviders\HostawayTimezoneProvider;
use HostawayAPI\Validators\PhonebookItemValidator;
use Phalcon\Cache\AdapterFactory as CacheAdapterFactory;
use Phalcon\Cache\CacheFactory;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Response;
use Phalcon\Logger\AdapterFactory as LoggerAdapterFactory;
use Phalcon\Logger\LoggerFactory;
use Phalcon\Storage\SerializerFactory;

$container = new FactoryDefault();
$container->setShared('response', function() {
    $response = new Response();
    $response->setContentType('application/json', 'utf-8');

    return $response;
});
$container->setShared('config', $config);

$container->set('logger', function () use ($config) {
    $adapterFactory = new LoggerAdapterFactory();
    $loggerFactory = new LoggerFactory($adapterFactory);

    return $loggerFactory->load($config->logger);
});

$container->set('db', function () use ($config) {
    if (mb_strtolower($config->database->adapter) === 'mysql') {
        return new Mysql([
            'host' => $config->database->host,
            'username' => $config->database->username,
            'password' => $config->database->password,
            'dbname' => $config->database->dbname,
            'port' => $config->database->port,
        ]);
    }

    throw new UnexpectedValueException('Unsupported Database Adapter');
});

$container->set('hostawayCountryProvider', function () use ($config) {
    $serializerFactory = new SerializerFactory();
    $adapterFactory = new CacheAdapterFactory(
        $serializerFactory,
        [
            'defaultSerializer' => 'Php',
            'lifetime' => 86400,
        ]
    );
    $cacheFactory = new CacheFactory($adapterFactory);
    $cacheProvider = $cacheFactory->load($config->cache);

    return new HostawayCountryProvider(
        $cacheProvider,
        new HttpClient()
    );
});

$container->set('hostawayTimezoneProvider', function () use ($config) {
    $serializerFactory = new SerializerFactory();
    $adapterFactory = new CacheAdapterFactory(
        $serializerFactory,
        [
            'defaultSerializer' => 'Php',
            'lifetime' => 86400,
        ]
    );
    $cacheFactory = new CacheFactory($adapterFactory);
    $cacheProvider = $cacheFactory->load($config->cache);

    return new HostawayTimezoneProvider(
        $cacheProvider,
        new HttpClient()
    );
});

$container->set('phonebookItemValidator', function () use ($container) {
    return new PhonebookItemValidator(
        $container->get('hostawayCountryProvider'),
        $container->get('hostawayTimezoneProvider')
    );
});

$container->set('createPhonebookItem', function () use ($container) {
    return new CreatePhonebookItem($container->get('phonebookItemValidator'));
});

$container->set('deletePhonebookItem', function () {
    return new DeletePhonebookItem();
});

$container->set('listPhonebookItem', function () {
    return new ListPhonebookItem();
});

$container->set('modifyPhonebookItem', function () use ($container) {
    return new ModifyPhonebookItem($container->get('phonebookItemValidator'));
});

$container->set('showPhonebookItem', function () {
    return new ShowPhonebookItem();
});

$container->set('upsertPhonebookItem', function () use ($container) {
    return new UpsertPhonebookItem($container->get('phonebookItemValidator'));
});

return $container;
