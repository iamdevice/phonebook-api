<?php

declare(strict_types=1);

namespace HostawayAPI\Actions;

use HostawayAPI\Exceptions\EntityNotFound;
use HostawayAPI\Models\PhonebookItem;

final class ShowPhonebookItem
{
    public function handle(int $id): array
    {
        $phonebookItem = PhonebookItem::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                'id' => $id,
            ],
        ]);

        if (!$phonebookItem instanceof PhonebookItem) {
            throw new EntityNotFound('Entity Not Found');
        }

        return $phonebookItem->jsonSerialize();
    }
}
