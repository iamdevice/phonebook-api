<?php

declare(strict_types=1);

namespace HostawayAPI\Actions;

use HostawayAPI\Models\PhonebookItem;

final class ListPhonebookItem
{
    public function handle(int $page, int $limit): array
    {
        $totalRecords = PhonebookItem::count();
        $phonebookItemList = PhonebookItem::find([
            'limit' => $limit,
            'offset' => ($page - 1) * $limit,
            'order' => 'id ASC'
        ]);

        return [
            'metadata' => [
                'page' => $page,
                'limit' => $limit,
                'currentRecords' => count($phonebookItemList),
                'totalRecords' => $totalRecords,
            ],
            'phonebookItems' => $phonebookItemList->jsonSerialize(),
        ];
    }
}
