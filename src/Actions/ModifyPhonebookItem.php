<?php

declare(strict_types=1);

namespace HostawayAPI\Actions;

use HostawayAPI\Exceptions\EntityNotFound;
use HostawayAPI\Models\PhonebookItem;
use HostawayAPI\Validators\PhonebookItemValidator;
use InvalidArgumentException;

final class ModifyPhonebookItem
{
    private const ENTITY_FIELD_MAP = [
        'firstName' => 'first_name',
        'lastName' => 'last_name',
        'phoneNumber' => 'phone_number',
        'countryCode' => 'country_code',
        'timezone' => 'tz_name',
    ];

    private PhonebookItemValidator $validator;

    public function __construct(PhonebookItemValidator $validator)
    {
        $this->validator = $validator;
    }

    public function handle(int $id, array $payload): array
    {
        if (!$this->validator->isValid($payload, array_keys($payload))) {
            $errorMessage = json_encode($this->validator->getErrors(), JSON_THROW_ON_ERROR);

            throw new InvalidArgumentException($errorMessage);
        }

        $phonebookItem = PhonebookItem::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                'id' => $id,
            ],
        ]);
        if (!$phonebookItem instanceof PhonebookItem) {
            throw new EntityNotFound('Entity Not Found');
        }

        foreach ($payload as $key => $value) {
            if (isset(self::ENTITY_FIELD_MAP[$key])) {
                $phonebookItem->{self::ENTITY_FIELD_MAP[$key]} = $value;
            }
        }
        $phonebookItem->save();

        return $phonebookItem->jsonSerialize();
    }
}
