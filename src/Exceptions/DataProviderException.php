<?php

declare(strict_types=1);

namespace HostawayAPI\Exceptions;

use RuntimeException;

final class DataProviderException extends RuntimeException
{

}
