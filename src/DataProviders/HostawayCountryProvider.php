<?php

declare(strict_types=1);

namespace HostawayAPI\DataProviders;

final class HostawayCountryProvider extends CachedProvider
{
    protected const PROVIDER_URL = 'https://api.hostaway.com/countries';

    protected const CACHE_KEY = 'hostaway-countries';

    public function getNameByCountryCode(string $countryCode): ?string
    {
        $data = $this->fetchData();

        return $data[$countryCode] ?? null;
    }

    public function getCountryCodeList(): array
    {
        return array_keys($this->fetchData());
    }

    public function hasCountryByCode(string $countryCode): bool
    {
        $data = $this->fetchData();

        return array_key_exists($countryCode, $data);
    }
}
