<?php

declare(strict_types=1);

namespace HostawayAPI\DataProviders;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use HostawayAPI\Exceptions\DataProviderException;
use InvalidArgumentException;
use JsonException;
use Phalcon\Cache;

abstract class CachedProvider
{
    protected const PROVIDER_URL = null;

    protected const CACHE_KEY = null;

    private Cache $cacheProvider;

    private ClientInterface $httpClient;

    public function __construct(
        Cache $cacheProvider,
        ClientInterface $httpClient
    ) {
        $this->cacheProvider = $cacheProvider;
        $this->httpClient = $httpClient;
    }

    protected function fetchData(): array
    {
        if ($this->cacheProvider->has(static::CACHE_KEY)) {
            return (array) $this->cacheProvider->get(static::CACHE_KEY);
        }

        $response = $this->httpClient->request(
            'GET',
            static::PROVIDER_URL,
            [
                RequestOptions::HEADERS => [
                    'Content-Type' => 'application/json',
                ],
                RequestOptions::HTTP_ERRORS => false,
            ]
        );

        if ($response->getStatusCode() !== 200) {
            throw new DataProviderException('Not found country data');
        }

        try {
            $data = json_decode((string) $response->getBody(), true, 512, JSON_THROW_ON_ERROR);
            if ($data['status'] !== 'success') {
                throw new DataProviderException('Not found country data');
            }
        } catch (JsonException $e) {
            throw new InvalidArgumentException('Invalid country data');
        }

        $this->cacheProvider->set(static::CACHE_KEY, $data['result'], 86400);

        return $data['result'];
    }
}
