<?php

declare(strict_types=1);

namespace HostawayAPI\Models;

use DateTimeImmutable;
use DateTimeInterface;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

final class PhonebookItem extends Model
{
    private const DATE_MYSQL_FORMAT = 'Y-m-d H:i:s';

    public $id;

    public $first_name;

    public $last_name;

    public $phone_number;

    public $country_code;

    public $tz_name;

    protected $created_at;

    protected $updated_at;

    public function getCreatedAt(): DateTimeImmutable
    {
        if (!$this->created_at instanceof DateTimeImmutable) {
            return new DateTimeImmutable($this->created_at);
        }

        return $this->created_at;
    }

    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    public function getUpdatedAt(): ?DateTimeImmutable
    {
        if (!empty($this->updated_at) && !$this->updated_at instanceof DateTimeImmutable) {
            return new DateTimeImmutable($this->updated_at);
        }

        return $this->updated_at;
    }

    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function initialize(): void
    {
        $this->setSource('phonebook_item');

        $this->addBehavior(new Timestampable([
            'beforeCreate' => [
                'field' => 'created_at',
                'format' => 'Y-m-d H:i:s',
            ],
            'afterUpdate' => [
                'field' => 'updated_at',
                'format' => 'Y-m-d H:i:s',
            ],
        ]));
    }

    public function beforeSave(): void
    {
        $this->created_at = (new DateTimeImmutable($this->created_at ?? 'now'))->format(self::DATE_MYSQL_FORMAT);
        if (!empty($this->updated_at)) {
            $this->updated_at = (new DateTimeImmutable($this->updated_at))->format(self::DATE_MYSQL_FORMAT);
        }
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'phoneNumber' => $this->phone_number,
            'countryCode' => $this->country_code,
            'timezone' => $this->tz_name,
            'createdAt' => $this->getCreatedAt()->format(DATE_ATOM),
            'updatedAt' => $this->getUpdatedAt() instanceof DateTimeInterface
                ? $this->getUpdatedAt()->format(DATE_ATOM)
                : null,
        ];
    }
}
