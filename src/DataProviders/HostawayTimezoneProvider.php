<?php

declare(strict_types=1);

namespace HostawayAPI\DataProviders;

final class HostawayTimezoneProvider extends CachedProvider
{
    protected const PROVIDER_URL = 'https://api.hostaway.com/timezones';

    protected const CACHE_KEY = 'hostaway-timezones';

    public function hasTimezoneByName(string $timezoneName): bool
    {
        $data = $this->fetchData();

        return array_key_exists($timezoneName, $data);
    }

    public function getTimezoneList(): array
    {
        return array_keys($this->fetchData());
    }
}
