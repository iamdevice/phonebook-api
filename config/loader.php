<?php

declare(strict_types=1);

use Phalcon\Loader;

$loader = new Loader();
$loader->registerNamespaces([
    'HostawayAPI' => __DIR__ . '/../src/',
]);
$loader->register();
