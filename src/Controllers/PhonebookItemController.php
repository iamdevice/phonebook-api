<?php

declare(strict_types=1);

namespace HostawayAPI\Controllers;

use Phalcon\Mvc\Controller;

final class PhonebookItemController extends Controller
{
    public function list(): array
    {
        $page = $this->request->get('page') ?? 1;
        $limit = $this->request->get('limit') ?? 25;

        return $this->listPhonebookItem->handle((int) $page, (int) $limit);
    }

    public function show(int $id): array
    {
        return $this->showPhonebookItem->handle($id);
    }

    public function create(): array
    {
        $payload = $this->request->getJsonRawBody(true);

        return $this->createPhonebookItem->handle($payload);
    }

    public function upsert(int $id): array
    {
        $payload = $this->request->getJsonRawBody(true);

        return $this->upsertPhonebookItem->handle($id, $payload);
    }

    public function modify(int $id): array
    {
        $payload = $this->request->getJsonRawBody(true);

        return $this->modifyPhonebookItem->handle($id, $payload);
    }

    public function delete(int $id): array
    {
        $this->deletePhonebookItem->handle($id);

        return [];
    }
}
